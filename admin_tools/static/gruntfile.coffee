module.exports = (grunt) ->

    # Project configuration.
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json')

        less:
            frontend:
                files:
                    'admin/css/base.css': 'admin/less/base.less'

        cssmin:
            frontend:
                files:
                    'admin/css/base.min.css': 'admin/css/base.css'

        watch:
            frontend_styles:
                files: ['admin/less/*.less'],
                tasks: ['frontend-styles']

    })
    
    # Plugins
    grunt.loadNpmTasks 'grunt-contrib-concat'
    grunt.loadNpmTasks 'grunt-contrib-cssmin'
    grunt.loadNpmTasks 'grunt-contrib-less'
    grunt.loadNpmTasks 'grunt-contrib-watch'

    # Frontend processing
    grunt.registerTask 'frontend-styles', ['less:frontend', 'cssmin:frontend']

    # Watch
    grunt.registerTask 'watch-frontend-styles', ['watch:frontend_styles']

    