$(document).ready(function(){
    var togglers = $('input[type=checkbox][data-toggle]');

    if(togglers.length > 0){
        togglers.change(function(e){
            // Get the classes/ids to toggle
            classes = $(this).attr('data-toggle').split(' ');
            status = $(this).is(':checked')
            $.each(classes, function(index, c){
                if(status == 'true'){
                    $(c).slideDown();
                }else{
                    $(c).slideUp();
                }
                
            })
        });
        // Set show/hide depending on checkbox status
        $.each(togglers, function(index, c){
            classes = $(c).attr('data-toggle').split(' ');
            status = $(c).is(':checked');

            $.each(classes, function(index, c){
                if(status == 'true'){
                    $(c).show();
                }else{
                    $(c).hide();
                }
                
            })
            
        })
    }
    
});