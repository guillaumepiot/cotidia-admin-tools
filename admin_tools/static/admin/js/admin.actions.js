$(document).ready(function(){
    // Timer to temper response
    var timer_position;
    var call_position;

    function checkPosition(){

        var scrollHeight = $(document).height()
        var windowHeight = $(window).height()
        var scrollTop    = $(window).scrollTop()
        var offset       = {top:0, bottom:80}
        var offsetTop    = offset.top
        var offsetBottom = offset.bottom

        if (typeof offset != 'object')         offsetBottom = offsetTop = offset
        if (typeof offsetTop == 'function')    offsetTop    = offset.top()
        if (typeof offsetBottom == 'function') offsetBottom = offset.bottom()

        if((scrollHeight - scrollTop - windowHeight) > offset.bottom){
            // if (call_position) { call_position.abort() } // If there is an existing XHR, abort it.
            // clearTimeout(timer_position); // Clear the timer so we don't end up with dupes.
            timer_position = setTimeout(function() { // assign timer a new timeout 
                    call_position = align_actions();
            }, 250); // 500ms delay, tweak for faster/slower
        }else{
            // if (call_position) { call_position.abort() } // If there is an existing XHR, abort it.
            // clearTimeout(timer_position); // Clear the timer so we don't end up with dupes.
            timer_position = setTimeout(function() { // assign timer a new timeout 
                    call_position = unalign_actions();
            }, 10); // 500ms delay, tweak for faster/slower
        }
    }

    function align_actions(){
    	$('.form-actions').addClass('fixed_bottom')
    }
    function unalign_actions(){
    	$('.form-actions').removeClass('fixed_bottom')
    }

    // Window loading default
    checkPosition();

    $(window).scroll(function(e){
        if (call_position) { call_position.abort() } // If there is an existing XHR, abort it.
        clearTimeout(timer_position);
        checkPosition();
    });

    // Filter toggle

    function toggle_filter(){
        $('#changelist-filter').toggle();
    }

    $('.btn-filter').click(function(){
        toggle_filter();
    })
});