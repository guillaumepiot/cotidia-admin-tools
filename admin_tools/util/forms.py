from django import forms
from django.utils.translation import ugettext_lazy as _

class RequiredBaseInlineFormSet(forms.models.BaseInlineFormSet):
	"""
	Won't allow formset_factory to be submitted with no forms
	"""
	def clean(self):
		#print 'length forms'
		#print self.forms, len(self.forms[0].cleaned_data.values())
		#print len(self.forms), hasattr(self.forms[0], "cleaned_data"), len(self.forms[0].cleaned_data.values()), self.forms[0].is_valid()
		if len(self.forms) > 0 and hasattr(self.forms[0], "cleaned_data") and len(self.forms[0].cleaned_data.values()) == 0:
			raise forms.ValidationError(_("Please enter at least one."))