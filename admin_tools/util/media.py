import os, hashlib, random
from django.template.defaultfilters import slugify

def get_media_upload_to(filename, location):
    
    fileName, fileExtension = os.path.splitext(filename)
    
    sha = hashlib.new('sha1')
    sha.update(str(random.random()))
    salt = sha.hexdigest()[:5]
    
    sha = hashlib.new('sha1')
    sha.update('%s%s' % (salt, slugify(fileName)))
    newname = sha.hexdigest()
    
    return '%s/%s%s' % (location, newname, fileExtension)