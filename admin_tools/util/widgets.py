from django.forms.widgets import CheckboxInput, boolean_check
from django.utils.translation import ugettext as _
from django.forms.utils import flatatt
from django.utils.html import conditional_escape, format_html

class ToggleCheckbox(CheckboxInput):

    # the 'toggle' argument should be a list of classes or id to toggle
    # eg: bool_field = forms.BooleanField(widget=ToggleCheckbox(toggle="#lightingequipment_set-group"))

    def __init__(self, attrs=None, check_test=None, toggle=None):
        super(ToggleCheckbox, self).__init__(attrs, toggle)
        self.toggle = toggle
        self.check_test = boolean_check if check_test is None else check_test

    def render(self, name, value, attrs=None):
        final_attrs = self.build_attrs(attrs, type='checkbox', name=name)
        if self.check_test(value):
            final_attrs['checked'] = 'checked'
        if not (value is True or value is False or value is None or value == ''):
            # Only add the 'value' attribute if a value is non-empty.
            final_attrs['value'] = force_text(value)
        return format_html('<input{0} data-toggle="%s"/>' % self.toggle, flatatt(final_attrs))

    class Media:
        # css = {
        #     'all': ('css/jquery.fileupload-ui.css', 'css/lightbox.css')
        # }
        js = (
            'admin/js/checkbox.toggle.js',)